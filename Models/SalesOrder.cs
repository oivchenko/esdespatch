﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Collections.ObjectModel;
using System.Windows;
using ESDespatch.Tools;

namespace ESDespatch.Models
{
    public  class SalesOrd_Hdr: PropertyChangedBase
    {
        public int seqno { get; set; }
        public int status { get; set; }
        public int accno { get; set; }
        public DateTime orderdate { get; set; }
        public DateTime duedate { get; set; }
        public string custorderno { get; set; }
        public string name { get; set; }
      
        
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string address4 { get; set; }

        public string street    { get { return address1; } }
        public string city      { get { return address2; } }
        public string state     { get { return address3; } }
        public string country   { get { return address4; } }


        public string instructions { get; set; }
        public decimal subtotal { get; set; }
        public decimal taxtotal { get; set; }
        public decimal salesno { get; set; }

        public bool backorder { get; set; }
        public int?  manifest  { get; set; }


        private bool _linesloaded = false;
        private ObservableCollection<SalesOrd_Lines> _lines = new ObservableCollection<SalesOrd_Lines>();
        public ObservableCollection<SalesOrd_Lines> Lines
        {
            get { return _lines; }
            set { _lines = value; NotifyOfPropertyChange(() => Lines); }

        }




        private bool _detailexposed=false;
        private void LoadLines()
        { 
            
        }

        public bool DetailExposed { get { return _detailexposed; } 
            set {
                _detailexposed = value;
                if (!_linesloaded) 
                { 
                    Lines = new ObservableCollection<SalesOrd_Lines>(TestData.getLinesForSo(this.seqno));
                    _linesloaded = true;
                }
                NotifyOfPropertyChange(() => DetailVisible);
                NotifyOfPropertyChange(() => DetailMarker);
            } 
        
        }
        public string     DetailMarker { get { return DetailExposed ? "-" : "+"; }  }
        public Visibility DetailVisible { get { return DetailExposed ? Visibility.Visible : Visibility.Collapsed; } }



        public SalesOrd_Hdr() { }





    }

    public class SalesOrd_Lines 
    {
        public int seqno { get; set; }
        public int accno { get; set; }
        public int hdr_seqno { get; set; }
        public string stockcode { get; set; }
        public string description { get; set; }
        public decimal ord_quant { get; set; }
        public decimal sup_quant { get; set; }
        public decimal inv_quant { get; set; }
        public decimal unitprice { get; set; }
        public decimal listprice { get; set; }
        public decimal linetotal { get; set; }
        public string si_status  { get; set; }
    }
}
