﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Collections.ObjectModel;
using ESDespatch.Tools;

namespace ESDespatch.Models
{
    public class Manifest:PropertyChangedBase
    {
        public DateTime transdate   { get; set; }
        public string   carriercode { get; set; }
        public string   details     { get; set; }
        public int manifestno { get; set; }

/*
        private int? _manifestno = null;
        public int? manifestno { get { return _manifestno; }
            set { _manifestno = value;
            LoadSO();
            NotifyOfPropertyChange(() => incSO);
            NotifyOfPropertyChange(() => exSO);
            } 
        }
*/

        private bool _expanded = false;
        public bool Expanded { 
            get { return _expanded; } 
            set { 
                    _expanded = value; 
                    NotifyOfPropertyChange(()=>Expanded); 
            } 
        }


        private bool _SOloaded = false;
        public bool SOloaded { get { return _SOloaded; } set { _SOloaded = value; } }

        private ObservableCollection<SalesOrd_Hdr> _incSO = new ObservableCollection<SalesOrd_Hdr>();
        public ObservableCollection<SalesOrd_Hdr> incSO { get { return _incSO; } set { _incSO = value; NotifyOfPropertyChange(() => incSO); } }

        private ObservableCollection<SalesOrd_Hdr> _exSO = new ObservableCollection<SalesOrd_Hdr>();
        public ObservableCollection<SalesOrd_Hdr> exSO { get { return _exSO; } set { _exSO = value; NotifyOfPropertyChange(() => exSO); } }


        public   Manifest() {  }


        public Manifest ShallowCopy()
        {
            return (Manifest)this.MemberwiseClone();
        }

        public void  LoadSO()
        {
            incSO.Clear();
            incSO = new ObservableCollection<SalesOrd_Hdr>(TestData.getIncludedSoForManifest(this.manifestno));
            exSO.Clear();
            exSO = new ObservableCollection<SalesOrd_Hdr>(TestData.getExludedSoForManifest(this.manifestno));
            NotifyOfPropertyChange(() => exSO);
            NotifyOfPropertyChange(() => incSO);
        }
    }
}
