﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Collections.ObjectModel;

using ESDespatch.Models;

namespace ESDespatch.Tools
{
    public  class TestData
    {

        private static    Collection<Manifest>  allmanifestos=
            new Collection<Manifest>(new Manifest[] { 
                new Manifest(){ manifestno=1, transdate=DateTime.Now.AddDays(-5), carriercode="aasd", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=2, transdate=DateTime.Now.AddDays(-7), carriercode="aaww", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=3, transdate=DateTime.Now.AddDays(-2), carriercode="bbww", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=4, transdate=DateTime.Now.AddDays(-2), carriercode="bbww", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=5, transdate=DateTime.Now.AddDays(-1), carriercode="xxyy", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=6, transdate=DateTime.Now.AddDays(-10), carriercode="zzxx", details="asdsd sdsd dsd"   },
                new Manifest(){ manifestno=7, transdate=DateTime.Now.AddDays(-8), carriercode="yyzz", details="asdsd sdsd dsd"   }
            
            });


        private static Collection<SalesOrd_Hdr> allSoHdr =
            new Collection<SalesOrd_Hdr>(
                new SalesOrd_Hdr[]{
                new SalesOrd_Hdr() { seqno = 1, manifest=1 ,orderdate = DateTime.Now.AddDays(-5), duedate = DateTime.Now.AddDays(5), accno = 14, name = "TERRY'S SPARES" },
                new SalesOrd_Hdr() { seqno = 2, manifest=1 ,orderdate = DateTime.Now.AddDays(-2), duedate = DateTime.Now.AddDays(2), accno = 9, name = "MARKET PARTS" },
                new SalesOrd_Hdr() { seqno = 3, orderdate = DateTime.Now.AddDays(-5), duedate = DateTime.Now.AddDays(2), accno = 10, name = "COMFORT AUTOMOTIVE SERVICES PTE LTD" },
                new SalesOrd_Hdr() { seqno = 4, manifest=3 ,orderdate = DateTime.Now.AddDays(-4), duedate = DateTime.Now.AddDays(1), accno = 12, name = "AUSSIE SPARES" },
                new SalesOrd_Hdr() { seqno = 5, orderdate = DateTime.Now.AddDays(-4), duedate = DateTime.Now.AddDays(1), accno = 12, name = "ALLPARTS AUTOMOTIVE LTD" },

                });

        private static Collection<SalesOrd_Lines> allSoLines =
            new Collection<SalesOrd_Lines>(
                
                new SalesOrd_Lines[]{
                
                new  SalesOrd_Lines(){ accno=1, hdr_seqno=55, linetotal=33.44M },
                new  SalesOrd_Lines(){ accno=1, hdr_seqno=55, linetotal=33.44M },
                new  SalesOrd_Lines(){ accno=1, hdr_seqno=55, linetotal=33.44M },
                new  SalesOrd_Lines(){ accno=1, hdr_seqno=55, linetotal=33.44M },
                new  SalesOrd_Lines(){ accno=1, hdr_seqno=55, linetotal=33.44M }
                
                }
                
                );


        public static Manifest[] genManifestos()
        {
            return allmanifestos.ToArray();
        }

        public static SalesOrd_Hdr[] genSOs()
        {
            return allSoHdr.ToArray();
        }


        public static Collection<SalesOrd_Hdr> getIncludedSoForManifest(int manifestno)
        {
            var tmp = allSoHdr.Where(t => t.manifest == manifestno);
            Collection<SalesOrd_Hdr> result = new Collection<SalesOrd_Hdr>(tmp.ToArray());
            return result;
        }

        public static Collection<SalesOrd_Hdr> getExludedSoForManifest(int manifestno)
        {
            var tmp = allSoHdr.Where(t => t.manifest != manifestno);
            Collection<SalesOrd_Hdr> result = new Collection<SalesOrd_Hdr>(tmp.ToArray());
            return result;
        }

        public static Collection<SalesOrd_Lines> getLinesForSo(int seqno)
        {
            return allSoLines;
        
        }

    }
}
