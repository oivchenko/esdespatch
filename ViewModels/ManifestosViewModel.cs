﻿using Caliburn.Micro;
using ESDespatch.Models;
using ESDespatch.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;



namespace ESDespatch.ViewModels
{

    public enum ManConst
    {
        [Description("EnterName")]
        EnterName,
        [Description("All")]
        AllManifests,
        [Description("Edit")]
        EditManifest,
        [Description("New")]
        NewManifest
    }


    public class person
    {
        public string name { get; set; }
        public string fio { get; set; }
        public person(string n, string f) { name = n; fio = f; }

    }


    public class ManifestosViewModel : PropertyChangedBase
    {
        private ManConst _state = ManConst.EnterName;
        public ManConst State
        {
            get { return _state; }
            set
            {
                _state = value;
                NotifyOfPropertyChange(() => State);
                NotifyOfPropertyChange(() => SubView);
            }
        }
        public string SubView
        {
            get { return Util.GetEnumDescription(_state); }
        }


        private Manifest _editedManifest=new Manifest();
        public  Manifest   editedManifest
        { 
            get{ return _editedManifest;} 
            set{_editedManifest=value;  NotifyOfPropertyChange(()=>editedManifest);}
        }


        private bool _name_entered=false;
        
        public bool NameEntered 
        {
            get { return _name_entered; }
            set { 
                    _name_entered=value;
                    NotifyOfPropertyChange(() => NameEntered);
                    NotifyOfPropertyChange(() => NameNoEntered);

                    NotifyOfPropertyChange(() => EnterNameVisibility);
                    NotifyOfPropertyChange(() => ClearNameVisibility);
                
                    NotifyOfPropertyChange(() => CanToEnterName);
                    NotifyOfPropertyChange(() => CanClearName);
            }
        }

        public Visibility EnterNameVisibility
        {
            get { return _name_entered ? Visibility.Collapsed : Visibility.Visible; }
        }

        public Visibility ClearNameVisibility
        {
            get { return _name_entered ?  Visibility.Visible:Visibility.Collapsed; }
        }


        public bool NameNoEntered
        {
            get { return !_name_entered; }
        }

        private string _username=string.Empty;
        public string UserName
        {
            get { return _username; }
            set { _username = (value == null) ? string.Empty : value.Trim();
            NotifyOfPropertyChange(() => UserName);
            NotifyOfPropertyChange(() => CanToEnterName);
            NotifyOfPropertyChange(() => CanClearName);
            }
        }

        public bool CanToEnterName { get { return  !string.IsNullOrEmpty(_username); } }
        public void ToEnterName() { State = ManConst.EnterName; NameEntered = true; ToAllManifestos(); }

        public bool CanClearName { get { return NameEntered && !string.IsNullOrEmpty(_username); } }
        public void ClearName() { State = ManConst.EnterName; NameEntered = false; UserName = ""; }

        public void ToAllManifestos() { State = ManConst.AllManifests;  }
        public void ToEditManifest(Manifest man)    
        { 
            State = ManConst.EditManifest;
//            editedManifest = man;
            editedManifest = man.ShallowCopy();
            editedManifest.LoadSO();
        }


//        public void ShowDetailSO(SalesOrd_Hdr order)
        public void ShowDetailSO(SalesOrd_Hdr order)
        {
//            int a=55;
//            MessageBox.Show(order.name);
            order.DetailExposed = !order.DetailExposed;
        }
        public void ToNewManifest()     { State = ManConst.NewManifest; }


        private ObservableCollection<Manifest> _manifests = new ObservableCollection<Manifest>();
        public ObservableCollection<Manifest> Manifests { get { return _manifests; } set { _manifests = value; NotifyOfPropertyChange(() => Manifests); } }

        private ObservableCollection<SalesOrd_Hdr> _SOs = new ObservableCollection<SalesOrd_Hdr>();
        public ObservableCollection<SalesOrd_Hdr> SOs { get { return _SOs; } set { _SOs = value; NotifyOfPropertyChange( ()=> SOs); } }

        public ManifestosViewModel()
        {
           Manifests = new ObservableCollection<Manifest>(TestData.genManifestos());
           SOs = new ObservableCollection<SalesOrd_Hdr>(TestData.genSOs());
        }




        public void ExcludeFromManifest(SalesOrd_Hdr order)
        {
//            MessageBox.Show(order.seqno.ToString());
            order.manifest = null;
            editedManifest.LoadSO();
        }


        public void includeToManifest(SalesOrd_Hdr order)
        {
//            MessageBox.Show(order.seqno.ToString());
            order.manifest = editedManifest.manifestno;
            editedManifest.LoadSO();
        }


    }
}
