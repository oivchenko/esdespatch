﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace ESDespatch.ViewModels
{
    public class TestViewModel : Screen
    {
        public AppViewModel model { get; set; }
        public TestViewModel() { State = "One"; }
        public TestViewModel(string state) { State = state; }
        public string tyta { get; set; }

        private string _state;
        public string State { get { return _state; } set { _state = value; NotifyOfPropertyChange(() => State); } }

        public  void setState(){ State="Two";}

        public AppViewModel app;
    }
}
