﻿using Caliburn.Micro;
using System.ComponentModel.Composition;
using System.Windows;


namespace ESDespatch.ViewModels
{
    [Export(typeof(AppViewModel))]
    public class AppViewModel : Conductor<object>     //PropertyChangedBase
    {

        private readonly IWindowManager _windowManager;

 
        private ManifestosViewModel _manifestos;
        public ManifestosViewModel  Manifestos 
        { 
            get { return _manifestos; } 
            set { _manifestos = value;
                    NotifyOfPropertyChange(() => Manifestos);
                }
        }


        public void TestMan()
        {
//            MessageBox.Show("ssssss");
            _windowManager.ShowDialog(new ManifestosViewModel(),"All");
       
        }

        [ImportingConstructor]
        public AppViewModel(IEventAggregator events, IWindowManager windowManager)
        {
            this.DisplayName = "EntroStyle Despatch Management";
            _windowManager = windowManager;
            Manifestos=new ManifestosViewModel();
            events.Subscribe(this);
        }

        public void ExitMan() { TryClose(); }


    }
}
